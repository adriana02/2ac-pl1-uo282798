#include <stdio.h>
#include <string.>

int copy(char *source, char* destination, unsigned int lengthDestination) {
	int i;
	for (i = 0; i<lengthDestination; i++) {
		destination[i] = source[i];
		// string ends with 0
		if (destination[i] == 0)
			break;
	}
	return i;
}


int main() {
	char src[30];
	char dest[30]
	strcpy(src, "This is a trial");
	int res = copy(src, dest, 30);
	printf("%s; copiados : %d", dest, res);
	return 0;
}
