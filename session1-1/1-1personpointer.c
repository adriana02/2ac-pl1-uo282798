#include <stdio.h>
#include <string.h>

struct _Person
{
	char name[30];
	int heightcm;
	double weightkg;
};

// abreviate the type name
typedef struct _Person Person;
typedef Person* PPerson;

int main(int argc, char* argv[])
{
	Person Javier;
	PPerson pJavier;

	// memory location of variable Javier is assigned to the pointer
	pJavier = &Javier;
	Javier.heightcm = 180;
	Javier.weightkg = 84.0;
	pJavier->weightkg = 83.2;
	return 0;
}
