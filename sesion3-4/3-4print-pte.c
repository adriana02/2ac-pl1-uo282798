#include <stdio.h>
#include <atc/linmem.h>
<<<<<<< HEAD
#include "3-4print-pte.h"
=======

//#include "3-4print-pte.h"
>>>>>>> c8a4c7c8ea109e52e8195f8d6c437e60aafb1736

// Given a virtual address, its virtual address is displayed on the screen,
// as well as its associated page table entry (pte), physical address and
// flags of the memory page. On top of this information, the title passed
// as argument is also displayed.
void print_virtual_physical_pte(void *virtual_addr, char *title)
{
	unsigned int physical_addr;
	unsigned int pte;
	unsigned int flags_vp;
	unsigned int frame_pte;
<<<<<<< HEAD
=======
	unsigned int offset_virtual;
>>>>>>> c8a4c7c8ea109e52e8195f8d6c437e60aafb1736

	printf("\n%s\n", title); // Print the title

	// Get the page table entry for the virtual address
	if (get_pte(virtual_addr, &pte))
	{
		perror("Linmem module error");
		return;
	}

	// If the page is in memory (i.e, if the present bit is one)
	if ((pte & 0x00000001) == 1)
	{
		// Store the flags associated to the memory page
<<<<<<< HEAD

		// OFFSET of physical address = OFFSET of virtual address
		// Take the FLAGS (offset of the virtual page)
		flags_vp = ((unsigned int) virtual_addr)&0xFFF;

		// FRAME of physical address = FRAME of pte
		// Take the FRAME of the pte: pte (bitwise and) inversion of 0xFFF
		frame_pte  = (pte)&~(0xFFF);

		// Calculate the memory physical address
		physical_addr = (frame_pte) + (flags_vp);
		
=======
		// OFFSET of physical address -> OFFSET of virtual address
		flags_vp = (pte)&0xFFF;
		offset_virtual = ((unsigned int) virtual_addr)&0xFFF;

		// Store the frame associated to the pte
		// FRAME of the physical address -> FRAME of pte
		// Frame of the pte: pte (bitwise and) inversion of 0xFFF
		frame_pte = (pte)&~(0xFFF);
		// Calculate the memory physical address
		physical_addr = (frame_pte) + (offset_virtual);

>>>>>>> c8a4c7c8ea109e52e8195f8d6c437e60aafb1736
		// Print address info
		printf("Virtual address: \t%.8Xh\n"
		       "Page Table Entry:\t%.8Xh\n"
		       "Physical Address:\t%.8Xh\n"
		       "Flags Virtual Page:\t%.3Xh\n",
		       (unsigned int)virtual_addr, pte, physical_addr, flags_vp);
	}
	else
	{
		fprintf(stderr, "Page %.5Xh has no page frame allocated\n",
			(unsigned int)virtual_addr >> 12);
	}
}
